package com.mybatis.enhance.store.utils;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sun.org.mozilla.javascript.internal.json.JsonParser;

import com.alibaba.fastjson.JSON;
import com.mybatis.enhance.store.manager.system.SysMysqlCreateTableManagerImpl;



/**
 * 输出sql脚本
 * 工具类
 * 
 * @author yilei
 * @version 20170713
 */
public class SqlTools {

	private static final Logger	log	= LoggerFactory.getLogger(SqlTools.class);
	/**
	 * @TODO  UNFINISH
	 */
	public static void writeSql(String sqlFileSaveDir,
			Map<String, List<Object>> newTableMap,
			Map<String, List<Object>> addTableMap,
			Map<String, List<Object>> modifyTableMap) {

		String newTablesql = createNewTableSql(newTableMap);

		String addAndModifySql = createAddAndModifyFiledSql(addTableMap,
				modifyTableMap);

		writeSqlFile(sqlFileSaveDir, newTablesql, addAndModifySql);
		

	}

	private static void writeSqlFile(String sqlFileSaveDir, String newTablesql,
			String addAndModifySql) {
		// TODO Auto-generated method stub

	}

	

	private static String createNewTableSql(
			Map<String, List<Object>> newTableMap) {
		if(null!=newTableMap&&newTableMap.size()>0){
			log.error("需要创建表:"+JSON.toJSONString(newTableMap));
		}

		return null;
	}
	
	private static String createAddAndModifyFiledSql(
			Map<String, List<Object>> addTableMap,
			Map<String, List<Object>> modifyTableMap) {
		if(null!=addTableMap&&addTableMap.size()>0){
			log.error("需要添加字段:"+JSON.toJSONString(addTableMap));
		}
		if(null!=addTableMap&&addTableMap.size()>0){
			log.error("需要更新字段:"+JSON.toJSONString(modifyTableMap));
		}
		return null;
	}
	

	public static void warnSql(Map<String, List<Object>> newTableMap,
			Map<String, List<Object>> addTableMap,
			Map<String, List<Object>> modifyTableMap,
			Map<String, List<Object>> removeTableMap) {
		if(null!=newTableMap&&newTableMap.size()>0){
			log.error("需要创建表:"+JSON.toJSONString(newTableMap));
		}
		if(null!=addTableMap&&addTableMap.size()>0){
			log.error("需要添加字段:"+JSON.toJSONString(addTableMap));
		}
		if(null!=addTableMap&&addTableMap.size()>0){
			log.error("需要更新字段:"+JSON.toJSONString(modifyTableMap));
		}
		if(null!=removeTableMap&&removeTableMap.size()>0){
			log.error("需要删除字段:"+JSON.toJSONString(removeTableMap));
		}
		
	}

}
