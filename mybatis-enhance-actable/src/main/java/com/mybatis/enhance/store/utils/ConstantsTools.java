package com.mybatis.enhance.store.utils;

import com.mybatis.enhance.store.constants.ConstantsEnum;

/**
 * 工具类
 * 
 * @author yilei
 * @version 20170713
 */
public class ConstantsTools {

	public static boolean createModel(String configModelLevel) {
		return configModelLevel.equalsIgnoreCase(ConstantsEnum.CREATE_MODEL
				.value());
	}

	public static boolean updateModel(String configModelLevel) {
		return configModelLevel.equalsIgnoreCase(ConstantsEnum.UPDATE_MODEL
				.value());
	}

	public static boolean updateBatchModel(String configModelLevel) {
		return configModelLevel
				.equalsIgnoreCase(ConstantsEnum.UPDATE_BATCH_MODEL.value());
	}

	public static boolean noneModel(String configModelLevel) {
		return configModelLevel.equalsIgnoreCase(ConstantsEnum.NONE_MODEL
				.value());
	}
	
	
	public static boolean sqlModel(String configModelLevel) {
		return configModelLevel.equalsIgnoreCase(ConstantsEnum.SQL_MODEL
				.value());
	}

}
