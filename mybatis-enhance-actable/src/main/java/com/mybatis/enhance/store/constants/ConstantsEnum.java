package com.mybatis.enhance.store.constants;

/**
 * 常量
 * 
 * @author yilei
 * @version 20170713
 */

public enum ConstantsEnum {
	CREATE_MODEL("create","初始化数据库，删除表主键字段等，重新创建表"),
	UPDATE_MODEL("update","仅执行更新脚本"),
	UPDATE_BATCH_MODEL("updatebatch","同一个表所有更新一次执行，提高效率"),
	SQL_MODEL("sql","仅输出sql"),
	NONE_MODEL("none","仅输出sql文件");
	
	//属性
	private final String value;

	//说明
	private final String desc;

	private ConstantsEnum(String value, String desc) {
		this.value = value;
		this.desc = desc;
	}

	public String value() {
		return this.value;
	}
	
	public String desc() {
		return this.desc;
	}

}
