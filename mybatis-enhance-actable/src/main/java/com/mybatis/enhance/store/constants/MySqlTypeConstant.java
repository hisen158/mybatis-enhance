package com.mybatis.enhance.store.constants;

import com.mybatis.enhance.store.annotation.LengthCount;


/**
 * 用于配置Mysql数据库中类型，并且该类型需要设置几个长度
 * 这里配置多少个类型决定了，创建表能使用多少类型
 * 例如：varchar(1)
 *     double(5,2)
 *     datetime
 * @LengthCount (0表示不需要设置，1表示需要设置一个，2表示需要设置两个))
 * @author sunchenbin
 * @version 2016年6月23日 下午5:59:33 
 */
public class MySqlTypeConstant {

	@LengthCount(LengthCount=0)
	public static final  String INT = "int";
	
	@LengthCount
	public static final  String VARCHAR = "varchar";
	
	@LengthCount(LengthCount=0)
	public static final  String TEXT = "text";
	@LengthCount(LengthCount=0)
	public static final  String MEDIUMTEXT = "mediumtext";
	
	
	@LengthCount(LengthCount=0)
	public static final  String DATETIME = "datetime";
	
	@LengthCount(LengthCount=2)
	public static final  String DECIMAL = "decimal";
	
	@LengthCount(LengthCount=2)
	public static final  String DOUBLE = "double";
	
	@LengthCount
	public static final  String CHAR = "char";
	
	/**
	 * 等于java中的long
	 */
	@LengthCount
	public static final  String BIGINT = "bigint";

	@LengthCount(LengthCount=0)
	public static final  String SMALLINT = "smallint";
	
	@LengthCount(LengthCount=0)
	public static final  String TIMESTAMP = "timestamp";
	
	@LengthCount(LengthCount=0)
	public static final  String INTEGER = "int";
	
	@LengthCount(LengthCount=0)
	public static final  String BIT = "bit";
	

	@LengthCount(LengthCount=0)
	public static final  String DATE = "date";

	
	/***
	 * 不需要判断长度
	 * @param LengthCount
	 * @return
	 */
	public static boolean noNeedLength(int LengthCount){
		return LengthCount==0;
	}
	
	/***
	 * 需要设置一个长度属性
	 * @param LengthCount
	 * @return
	 */
	public static boolean needOneLength(int LengthCount){
		return LengthCount==1;
	}
	
	/***
	 * 需要设置两个长度属性
	 * @param LengthCount
	 * @return
	 */
	public static boolean needTwoLength(int LengthCount){
		return LengthCount==2;
	}
	
	/***
	 * 有的字段类型不需要判断 默认值
	 */
	public static boolean noNeedCompareDefault(String  columnType){
		return columnType.equalsIgnoreCase(TIMESTAMP);
	}
}
