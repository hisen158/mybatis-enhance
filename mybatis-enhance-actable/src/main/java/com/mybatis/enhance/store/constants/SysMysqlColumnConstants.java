package com.mybatis.enhance.store.constants;

/**
 * 用于查询表中字段结构详细信息
 *
 * @author sunchenbin
 * @version 2016年6月23日 下午6:10:56 
 */
public class SysMysqlColumnConstants {

	/**
	 * 字段名
	 */
	public static final String COLUMN_NAME = "column_name";
	/**
	 * 默认值
	 */
	public static final String COLUMN_DEFAULT = "column_default";
	/**
	 * 是否可为null，值：(YES,NO)
	 */
	public static final String IS_NULLABLE = "is_nullable";
	/**
	 * 数据类型
	 */
	public static final String DATA_TYPE = "data_type";
	/**
	 * 长度，如果是0的话是null
	 */
	public static final String NUMERIC_PRECISION = "numeric_precision";
	/**
	 * 小数点数
	 */
	public static final String NUMERIC_SCALE = "numeric_scale";
	/**
	 * 是否为主键，是的话是PRI
	 */
	public static final String COLUMN_KEY = "column_key";
	/**
	 * 是否为自动增长，是的话为auto_increment
	 */
	public static final String EXTRA = "extra";

}
